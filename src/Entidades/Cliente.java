/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import tuber.types.Client;
/**
 *
 * @author agustinrodriguez
 */
public abstract class Cliente extends FichaPosicion implements Client {
    String nombre;
    String numero;
    int idCliente;
  //  boolean busy();
    public Cliente(String name, String number, float latC, float lonC, int id) {
     lat = latC;
     lon = lonC;
     nombre = name;
     idCliente = id;
     numero = number;
     symbol = 'C';
    }
    @Override
    public boolean busy(){
        return true;
    }
    
    @Override 
    public boolean querying() {
        return false;
    }
    
    public void pedir() {
        //se especifica punto 12
        double prob = 0.6;
        int maxturn = 20;
    if (Math.random() <= (maxturn*prob)) {
        busy() = true;

    }
}
}

