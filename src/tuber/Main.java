/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuber;

import java.awt.BorderLayout;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *
 * @author agustinrodriguez
 */
public class Main extends JFrame {
    
    private JTextField textField = new JTextField(30);
    private JButton button = new JButton("santiago");
    private JPanel panel = new JPanel();
    
    private GoogleApi googleApi = new GoogleApi();
    private String location = "santiago";
    private JLabel googleMap;
    

    
    public void setMap(String location){
        googleApi.downloadMap(location);
        googleMap = new JLabel(googleApi.getMap(location));
        googleApi.fileDelete(location);  
        add(BorderLayout.SOUTH, googleMap);
        pack();
    }
    
    public Main() {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Google Maps");
        setVisible(true);
        
        panel.add(textField);
        panel.add(button);
        
          googleApi.downloadMap(location);
        googleMap = new JLabel(googleApi.getMap(location));
        googleApi.fileDelete(location);  
        add(BorderLayout.SOUTH, googleMap);
        pack(); 
        
        add(BorderLayout.NORTH, panel);
        pack();
    }
    
}
